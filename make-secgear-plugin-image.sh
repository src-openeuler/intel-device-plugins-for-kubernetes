#######################################################################
##- @Copyright (C) Huawei Technologies., Ltd. 2021. All rights reserved.
# - lcr licensed under the Mulan PSL v2.
# - You can use this software according to the terms and conditions of the Mulan PSL v2.
# - You may obtain a copy of Mulan PSL v2 at:
# -     http://license.coscl.org.cn/MulanPSL2
# - THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# - IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# - PURPOSE.
# - See the Mulan PSL v2 for more details.
##- @Description: apply patchs
##- @Author: lifeng
##- @Create: 2021-05-25
#######################################################################
#!/bin/bash

set -ex

pkg=v0.20.0
cwd=$PWD
src=$cwd/intel-device-plugins-for-kubernetes-0.20.0

tar -xzvf $pkg.tar.gz

cd $src

cat $cwd/series.conf | while read line
do
    if [[ $line == '' || $line =~ ^\s*# ]]; then
        continue
    fi
    echo $cwd/$line
    patch -p1 -F1 -s < $cwd/$line
done


arch=`uname -m`

if [ "$arch" = "aarch64" ]; then
	make arm-itrustee-plugin
else
	make intel-sgx-plugin 
fi

cd $cwd
